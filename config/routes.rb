Rails.application.routes.draw do
  resources :posts
  resources :comments
  get  '/users', to: 'users#index'
  post '/login', to: 'authentication#login'
  post '/follow', to: 'follow#follow'
  post '/unfollow', to: 'follow#unfollow'
end
