# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(email: 'admin@admin.com', name: 'Admin', is_admin: true, password: '123456') if User.where(email: 'admin@admin.com').first.nil?
User.create(email: 'usuario@usuario.com', name: 'Usuário comum', password: '123456') if User.where(email: 'usuario@usuario.com').first.nil?
User.create(email: 'usuario2@usuario.com', name: 'Usuário comum 2', password: '123456') if User.where(email: 'usuario2@usuario.com').first.nil?
