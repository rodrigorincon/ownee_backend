class CreateUserFollowers < ActiveRecord::Migration[5.2]
  def change
    create_table :user_followers do |t|
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_belongs_to :user_followers, :follower, :class_name => 'User'
  end
end
