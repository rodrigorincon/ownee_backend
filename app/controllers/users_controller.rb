class UsersController < ApplicationController
  before_action :authorize_request
  
  # GET /users
  def index
  	@users = User.where.not(id: current_user.id).order(:id)
    render json: @users
  end

end
