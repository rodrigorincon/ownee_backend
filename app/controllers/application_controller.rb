class ApplicationController < ActionController::API
  include CanCan::ControllerAdditions

  rescue_from CanCan::AccessDenied do |exception|
    render json: { message: exception.message }, status: 403
  end

  def current_user
    @current_user
  end

  def authorize_request
    header = request.headers['Authorization']
    header = header.split(' ').last if header
    begin
      @decoded = JsonWebToken.decode(header)
      if Time.now <= @decoded[:exp]
        @current_user = User.find(@decoded[:user_id])
      else
        render json: { errors: 'acesso expirado! Favor logar de novo' }, status: :unauthorized  
      end
    rescue ActiveRecord::RecordNotFound => e 
      render json: { errors: e.message }, status: :unauthorized 
    rescue JWT::DecodeError => e 
      render json: { errors: e.message }, status: :unauthorized
    end
  end

end
