class CommentsController < ApplicationController
  before_action :authorize_request
  load_and_authorize_resource :comment

  # GET /comments
  def index
    render json: @comments
  end

  # GET /comments/1
  def show
    render json: @comment
  end

  # post /comments
  def create
    @comment.user_id = @current_user.id

    if @comment.save
      render json: @comment, status: :created, location: @comment
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /comments/1
  def update
    if @comment.update(comment_params)
      render json: @comment
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /comments/1
  def destroy
    @comment.soft_delete!
  end

  private
    # Only allow a trusted parameter "white list" through.
    def comment_params
      params.require(:comment).permit(:text, :post_id)
    end
end
