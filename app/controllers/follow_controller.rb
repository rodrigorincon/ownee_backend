class FollowController < ApplicationController
  before_action :authorize_request
  before_action :set_user
  load_and_authorize_resource :user_follower

  # POST /follow
  def follow
    another_user = User.find(follow_params[:another_user_id])
    if @user.follow_user(another_user)
      render json: @user
    else
      render json: 'Esse usuário já segue esta pessoa.', status: :unprocessable_entity
    end
  end

  # POST /unfollow
  def unfollow
    another_user = User.find(follow_params[:another_user_id])
    if @user.unfollow_user(another_user)
      render json: @user
    else
      render json: 'Esse usuário não segue esta pessoa.', status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = @current_user
    end

    # Only allow a trusted parameter "white list" through.
    def follow_params
      params.require(:follow).permit(:another_user_id)
    end

end
