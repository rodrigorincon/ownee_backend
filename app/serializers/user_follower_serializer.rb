class PostSerializer < ActiveModel::Serializer
  belongs_to :user
  belongs_to :follower
end