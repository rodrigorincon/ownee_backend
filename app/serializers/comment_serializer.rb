class CommentSerializer < ActiveModel::Serializer
  attributes :id, :text, :post_id, :created_at, :updated_at
  belongs_to :user

end