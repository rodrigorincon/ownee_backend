class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :is_admin, :is_followed_by_me

  def is_followed_by_me
    current_user.following(object)
  end

end