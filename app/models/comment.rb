class Comment < ApplicationRecord
  has_soft_deletion default_scope: true

  belongs_to :user
  belongs_to :post
end
