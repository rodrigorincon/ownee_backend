class UserFollower < ApplicationRecord
  belongs_to :user
  belongs_to :follower, :class_name => 'User'

  validates_uniqueness_of :follower_id, scope: [:user_id]
  
end
