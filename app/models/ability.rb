class Ability
  include CanCan::Ability

  def initialize(user)
    if User.exists?(user.id)
      if user.is_admin
        can :manage, :all
      else
        can :read,   Post, {user_id: user.who_i_follow.map{|followuser| followuser.id} } 
        can :manage, Post, {user_id: user.id}
        can :manage, UserFollower, {follower_id: user.id}
        can :create, Comment, {post: {user_id: user.id } }
        can :read,   Comment, {post: {user_id: user.id } }
        can [:read, :update, :destroy], Comment, {user_id: user.id}
        can :create, Comment, {post: {user_id: user.who_i_follow.map{|followuser| followuser.id} } }
      end
    end

  end
end
