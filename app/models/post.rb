class Post < ApplicationRecord
  has_soft_deletion default_scope: true
  
  belongs_to :user
  has_many :comments, dependent: :destroy

  validates :title, :user, presence: true
end
