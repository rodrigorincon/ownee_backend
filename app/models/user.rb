class User < ApplicationRecord
  has_secure_password
  has_soft_deletion default_scope: true
  
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy

  has_many :user_followers, :foreign_key => :user_id
  has_many :users, :through => :user_followers
  
  validates :email, :name, presence: true, absence: false
  validates :email, uniqueness: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates_inclusion_of :is_admin, in: [true, false]
  validates :password, length: { minimum: 6 }


  # current user follow another user
  def follow_user(another_user)
    follow = UserFollower.create(user_id: another_user.id, follower_id: self.id)
    UserFollower.exists?(follow.id)
  end

  # current user unfollow another user
  def unfollow_user(another_user)
    UserFollower.where(user_id: another_user.id, follower_id: self.id).delete_all > 0
  end

  # current user change from follow to unfollow (or the opposite) another user
  def follow_or_unfollow(another_user)
    userFollowers = UserFollower.where(user_id: another_user.id, follower_id: self.id)
    if userFollowers && userFollowers.size > 0
      userFollowers.delete_all
      return false
    else
      follow_user(another_user)
      return true
    end
  end

  def following(another_user)
    return !UserFollower.where(user_id: another_user.id, follower_id: self.id).empty?
  end

  def following_me(another_user)
    return !UserFollower.where(user_id: self.id, follower_id: another_user.id).empty?
  end

  def who_i_follow
    User.joins("INNER JOIN user_followers uf ON uf.user_id = users.id").where("uf.follower_id = ?", self.id)
  end


end
